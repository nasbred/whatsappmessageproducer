package model;

import io.quarkus.kafka.client.serialization.ObjectMapperSerializer;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class WhatsAppMessageSerializer implements Serializer<WhatsappMessage> {

    private final ObjectMapperSerializer<WhatsappMessage> objectMapperSerializer;

    public WhatsAppMessageSerializer() {
        objectMapperSerializer = new ObjectMapperSerializer<>();
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        objectMapperSerializer.configure(configs, isKey);
    }

    @Override
    public byte[] serialize(String topic, WhatsappMessage data) {
        return objectMapperSerializer.serialize(topic, data);
    }

    @Override
    public byte[] serialize(String topic, Headers headers, WhatsappMessage data) {
        return objectMapperSerializer.serialize(topic, headers, data);
    }

    @Override
    public void close() {
        objectMapperSerializer.close();
    }
}
