package model;

import lombok.With;
import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import ru.tsc.crm.communication.sender.domain.Attachment;
import ru.tsc.crm.communication.sender.domain.Recipient;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@With
@Data
@Builder
@NoArgsConstructor
public class WhatsappMessage {

    /**
     * Уникальный идентификатор сообщения.
     */
    public UUID msgId;

    /**
     * Пользовательские идентификаторы, если недостаточно msgId (опционально)
     */
    public Map<String, String> customTags;

    /**
     * Получатель
     */
    public Recipient recipient;

    /**
     * Отправитель (опционально)
     */
    public String sender;

    /**
     * Тело сообщения
     */
    public String text;

    /**
     * Список аттачментов (опционально)
     */
    public List<Attachment> attachments;

    public WhatsappMessage(
            UUID msgId,
            Map<String, String> customTags,
            Recipient recipient,
            String sender,
            String text,
            List<Attachment> attachments
    ) {
        this.msgId = msgId;
        this.customTags = customTags;
        this.recipient = recipient;
        this.sender = sender;
        this.text = text;
        this.attachments = attachments;
    }

    public WhatsappMessage(
            UUID msgId,
            Recipient recipient,
            String text
    ) {
        this.msgId = msgId;
        this.customTags = null;
        this.recipient = recipient;
        this.sender = null;
        this.text = text;
        this.attachments = null;
    }

    public WhatsappMessage(
            UUID msgId,
            Recipient recipient,
            List<Attachment> attachments
    ) {
        this.msgId = msgId;
        this.customTags = null;
        this.recipient = recipient;
        this.sender = null;
        this.text = null;
        this.attachments = attachments;
    }

}
