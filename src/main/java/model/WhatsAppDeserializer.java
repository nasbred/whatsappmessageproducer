package model;

import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class WhatsAppDeserializer implements Deserializer<WhatsappMessage> {

    private final ObjectMapperDeserializer<WhatsappMessage> objectMapperDeserializer;

    public WhatsAppDeserializer() {
        objectMapperDeserializer = new ObjectMapperDeserializer<>(WhatsappMessage.class);
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        objectMapperDeserializer.configure(configs, isKey);
    }

    @Override
    public WhatsappMessage deserialize(String topic, byte[] data) {
        return objectMapperDeserializer.deserialize(topic, data);
    }

    @Override
    public WhatsappMessage deserialize(String topic, Headers headers, byte[] data) {
        return objectMapperDeserializer.deserialize(topic, headers, data);
    }

    @Override
    public void close() {
        objectMapperDeserializer.close();
    }
}
