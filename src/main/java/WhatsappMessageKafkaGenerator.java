import io.smallrye.mutiny.Multi;
import io.smallrye.reactive.messaging.kafka.Record;
import model.WhatsappMessage;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import ru.tsc.crm.communication.sender.domain.Recipient;
import ru.tsc.crm.communication.sender.domain.RecipientType;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.UUID;


@ApplicationScoped
public class WhatsappMessageKafkaGenerator {

    private Recipient recipient = Recipient.builder()
            .type(RecipientType.MSISDN)
            .value("79670942797")
            .build();

    private WhatsappMessage message1 = new WhatsappMessage(
            UUID.randomUUID(),
            recipient,
            "Test"
    );

    private List<WhatsappMessage> list = List.of( message1);


    @Outgoing("wazzup-send")
    public Multi<Record<Long, WhatsappMessage>> email() {
        return Multi.createFrom().items(list.stream()
                .map(m -> Record.of(1L, m))
        );
    }

}
